## Defining abstraction within go 

Abstraction is simply used to focus on essential details while hiding unnecessary or intricate implementation details.

In this specific example we're using shapes to show off the concept. 
A shape in this case will always have an Area() function that allows the calculation of its area but it is a different implementation for circles and rectangles. So we define a shape interface that is extended within circles and rectangles.

We can run the example by running: 
`go run main.go` 