package main

import "fmt"

// Shape is an interface that defines a method for calculating area.
type Shape interface {
	Area() float64
}

// Circle is a struct representing a circle.
type Circle struct {
	Radius float64
}

// Rectangle is a struct representing a rectangle.
type Rectangle struct {
	Width  float64
	Height float64
}

// Implement the Area method for Circle.
func (c Circle) Area() float64 {
	return 3.14 * c.Radius * c.Radius
}

// Implement the Area method for Rectangle.
func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

func main() {
	// Create instances of Circle and Rectangle.
	circle := Circle{Radius: 5}
	rectangle := Rectangle{Width: 4, Height: 6}

	// Calculate and print the area of the shapes using the same interface.
	shapes := []Shape{circle, rectangle}
	for _, shape := range shapes {
		fmt.Printf("Area: %.2f\n", shape.Area())
	}
}
