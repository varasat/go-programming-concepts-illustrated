package main

import "fmt"

// Factorial calculates the factorial of a positive integer n using recursion.
// Eg: factorial of 4 is 24 because 4! = 4 x 3 x 2 x 1
func Factorial(n int) int {
	if n == 0 {
		return 1 // Base case: 0! = 1
	}
	return n * Factorial(n-1) // Recursive case: n! = n * (n-1)!
}

func main() {
	n := 5 // Calculate the factorial of 5
	result := Factorial(n)
	fmt.Printf("%d! = %d\n", n, result)
}
