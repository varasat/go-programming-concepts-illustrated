## Please use the abstraction example for this one

In this example, the Shape interface is open for extension, as you can add new shapes that implement the Area method without modifying the existing code (closed for implementation).