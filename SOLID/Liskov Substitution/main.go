package main

import "fmt"

// Bird is a base type.
type Bird struct {
	Name string
}

func (b Bird) Fly() {
	fmt.Printf("%s is flying.\n", b.Name)
}

// Ostrich is a subtype of Bird.
type Ostrich struct {
	Bird
}

func main() {
	ostrich := Ostrich{Bird{Name: "Ostrich"}}
	ostrich.Fly() // Although ostriches don't fly, this adheres to LSP as it's substituting a Bird.
}
