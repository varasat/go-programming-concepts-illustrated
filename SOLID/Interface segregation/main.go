package main

import "fmt"

/**
** In this example, the Worker interface follows ISP by having small, client-specific methods, and the Engineer and Manager types implement only the methods they need.
**/

// Worker interface follows ISP by having small, client-specific methods.
type Worker interface {
	Work()
	Eat()
}

// Engineer implements the Worker interface.
type Engineer struct {
	Name string
}

func (e Engineer) Work() {
	fmt.Printf("%s is working.\n", e.Name)
}

func (e Engineer) Eat() {
	fmt.Printf("%s is eating lunch.\n", e.Name)
}

// Manager implements the Worker interface.
type Manager struct {
	Name string
}

func (m Manager) Work() {
	fmt.Printf("%s is managing the team.\n", m.Name)
}

func (m Manager) Eat() {
	fmt.Printf("%s is eating lunch.\n", m.Name)
}

func main() {
	engineer := Engineer{Name: "Alice"}
	manager := Manager{Name: "Bob"}

	people := []Worker{engineer, manager} //since Engineer and manager both implement Work and Eat we can use them both as workers in the worker slice

	for _, person := range people {
		person.Work()
		person.Eat()
	}
}
