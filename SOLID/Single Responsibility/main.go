package main

import "fmt"

// Calculator has a single responsibility: performing calculations.
type Calculator struct{}

func (c Calculator) Add(a, b int) int {
	return a + b
}

func (c Calculator) Subtract(a, b int) int {
	return a - b
}

func main() {
	calculator := Calculator{}
	sum := calculator.Add(5, 3)
	diff := calculator.Subtract(5, 3)

	fmt.Printf("Sum: %d, Difference: %d\n", sum, diff)
}
