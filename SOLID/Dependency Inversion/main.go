package main

import "fmt"

/**
In this example, the client code depends on the Messenger abstraction, and it can switch between different concrete implementations (EmailService and SMSService) without changing the client code,
adhering to the Dependency Inversion Principle.
**/

// Messenger is an abstraction.
type Messenger interface {
	Send(message string)
}

// EmailService is a concrete implementation of Messenger.
type EmailService struct{}

func (e EmailService) Send(message string) {
	fmt.Printf("Email sent: %s\n", message)
}

// SMSService is another concrete implementation of Messenger.
type SMSService struct{}

func (s SMSService) Send(message string) {
	fmt.Printf("SMS sent: %s\n", message)
}

func main() {
	// Client code depends on the Messenger interface.
	var messenger Messenger

	messenger = EmailService{}
	messenger.Send("Hello, world!")

	messenger = SMSService{}
	messenger.Send("Hi there!")
}
